# Makefile for selection detection analyses
# First off, define some global variables:

# Run name - Set the name for this run
RUN_NAME=0001
# Original vcf filename
ORIGINAL_VCF=input_data/$(RUN_NAME)/snps.vcf.gz
# Sample coordinates file
SAMPLE_COORDS=$(wd)input_data/$(RUN_NAME)/coords.tsv
# Population map (individual to population matching)
POPMAP=input_data/$(RUN_NAME)/individual_pop_map.txt
# Number of threads to use on all the threaded analyses
threads=8
# Min "Minor Allele Count" allowed:
MAC=1
# Strategy for dealing with LD (CenterSNP or mindist)
LD_STRAT=CenterSNP
# Minimum distance between SNPs for dealing with LD (only used when LD_STRAT=mindist)
MINDIST=3000
# Final VCF filename (depends on LD startegy)
ifeq ($(LD_STRAT), CenterSNP)
	FINAL_VCF=$(OUTDIR)/$(RUN_NAME)_mac$(MAC)_biallele_CenterSNP.vcf
else ifeq ($(LD_STRAT), mindist)
	FINAL_VCF=$(OUTDIR)/$(RUN_NAME)_mac$(MAC)_biallele_mindist$(MINDIST).vcf
endif
# PCA explained variance threshold (for transforming env vars in PCA eigenvectors)
PCA_THRESHOLD=0.85
# FDR value for Bayescan analysis
bayescan_FDR=0.05
# Limit for significant SelEstim KLD values
selestim_limit=0.01
# Bayesfactor threshold for Baypass associations
ASSOC_BF=20
# Alpha value to consider LFMM associations as significant
LFMM_alpha=0.05
# Random seed for whatever requests it
SEED=112358
# Get the current working directory
wd=$(dir $(abspath Makefile))
# Bioclim data location
BIOCLIM=$(wd)mapdata
# Outputs location
OUTDIR=$(wd)output_data/$(RUN_NAME)

# Now, set the rules:
all : filtering outliers associations summary

# GO!
BIOCLIMS := $(shell seq 1 1 19| sed 's/^[[:digit:]]$$/0&/')

CHELSA := $(patsubst %,$(BIOCLIM)/CHELSA_bio10_%.tif,$(BIOCLIMS))


$(BIOCLIM)/CHELSA_bio10_%.tif :
	wget https://envicloud.os.zhdk.cloud.switch.ch/chelsa/chelsa_V1/climatologies/bio/$(shell echo "$@" | sed 's|.*/||' ) -O $@


$(OUTDIR)/Climdata_by_location.tsv : $(SAMPLE_COORDS) $(CHELSA)
	# Create new directory for results
	mkdir -p $(OUTDIR)/
	# Extract bioclimatic data from the samples' coordinates
	cut -f 1 $< > $(BIOCLIM)/temp_holder.tsv
	bin/extract_layer_data.sh $< $@ $(BIOCLIM)
	sed -i '1 s|$(BIOCLIM)/CHELSA_||g; 1 s|\.tif||g; s|bio10_|bio_|g' $@


$(OUTDIR)/Climdata_by_location_uncorr.tsv : $(OUTDIR)/Climdata_by_location.tsv $(SAMPLE_COORDS)
	# Remove correlated variables from present conditions file
	Rscript bin/variable_reduction.R $< $(PCA_THRESHOLD) $@


$(OUTDIR)/Climdata_by_location_uncorr_per_pop.tsv : $(OUTDIR)/Climdata_by_location_uncorr.tsv $(POPMAP)
	# Merge individual data into populaton data
	python3 bin/indiv_to_merged_pops.py $(word 2,$^) $(word 1,$^) > $@


$(OUTDIR)/ENVFILE : $(OUTDIR)/Climdata_by_location_uncorr_per_pop.tsv
	# Remove the column names, and transpose to have a Baypass compatible file
	cut -f 2- $< | awk -f bin/transpose.awk > $@


$(OUTDIR)/LFMM_ENVFILE : $(OUTDIR)/Climdata_by_location_uncorr.tsv
	# So simple...
	tail -n +2 $< | sed 's/[[:digit:]]\+\t/\t/' > $@


$(OUTDIR)/$(RUN_NAME)_mac$(MAC)_biallele.vcf : $(ORIGINAL_VCF)
	# Create new directory for results
	mkdir -p $(OUTDIR)/
	# Remove SNPs with very low MAF and more than biallelic
	bin/vcftools --gzvcf $(word 1,$^) --mac $(MAC) --out $@ --recode --max-alleles 2
	mv $@.recode.vcf $@


$(FINAL_VCF) : $(OUTDIR)/$(RUN_NAME)_mac$(MAC)_biallele.vcf
	# Keep only the Center SNP or keep a minimum distance between SNPs to avoid LD issues
	# Our final VCF file is now produced.
	if [ $(LD_STRAT) = "CenterSNP" ];\
	then\
		python3 bin/vcf_parser.py --center-snp -vcf $(word 1,$^);\
		mv $(subst .vcf,,$<)CenterSNP.vcf $@;\
	elif [ $(LD_STRAT) = "mindist" ];\
	then\
		python3 bin/vcf_parser.py --distance $(MINDIST) -vcf $(word 1,$^);\
	fi


$(OUTDIR)/individuals.txt : $(FINAL_VCF)
	# Create a file with the names of the individuals that were retained in the filtered VCF
	bin/get_indivs_from_vcf.sh $< $@


$(OUTDIR)/popinfo_names.txt : $(OUTDIR)/individuals.txt $(POPMAP)
	# Create a popinfo file based on the retained individuals only
	$(shell grep -f $< $(word 2,$^) > $@)


$(OUTDIR)/$(RUN_NAME)_popnames_single.txt : $(OUTDIR)/popinfo_names.txt
	# Create a file that contains only the names of the present populations
	$(shell cut -f 2 $< |uniq > $@)


$(OUTDIR)/Bayescan/$(RUN_NAME).geste : $(FINAL_VCF) $(OUTDIR)/popinfo_names.txt
	# Create a directory to store outputs
	mkdir -p $(OUTDIR)/Bayescan
	
	# Create a SPID from the skeleton
	sed 's|path_to_popfile|$(word 2,$^)|g' $(wd)skeletons/skeleton.spid > $(OUTDIR)/vcf2geste.spid
	echo "" >> $(OUTDIR)/vcf2geste.spid
	echo "WRITER_FORMAT=GESTE_BAYE_SCAN" >> $(OUTDIR)/vcf2geste.spid
	echo "" >> $(OUTDIR)/vcf2geste.spid
	echo "GESTE_BAYE_SCAN_WRITER_DATA_TYPE_QUESTION=SNP" >> $(OUTDIR)/vcf2geste.spid
	
	# Perform the file conversion
	java -Xmx8g -Xms512m -jar bin/PGDSpider2-cli.jar -inputfile $(word 1, $^) -inputformat VCF -outputfile $@ -outputformat GESTE -spid $(OUTDIR)/vcf2geste.spid


$(OUTDIR)/Bayescan/$(RUN_NAME)_outliers.txt : $(OUTDIR)/Bayescan/$(RUN_NAME).geste
	# We have to 'cd' due to the way bayescan outputs files
	cd $(OUTDIR)/Bayescan && $(wd)bin/bayescan -snp -threads $(threads) $<
	Rscript -e 'source("bin/plot_R.r"); plot_bayescan("$(OUTDIR)/Bayescan/$(RUN_NAME).g_fst.txt", FDR=$(bayescan_FDR))' > $@


$(OUTDIR)/SelEstim/$(RUN_NAME).selestim : $(OUTDIR)/Bayescan/$(RUN_NAME).geste
	# Create a directory to store outputs
	mkdir -p $(OUTDIR)/SelEstim
	
	# Perform the conversion
	bash bin/GESTE2SelEstim.sh $< $@


$(OUTDIR)/SelEstim/$(RUN_NAME)_outliers.txt : $(OUTDIR)/SelEstim/$(RUN_NAME).selestim
	# Run SelEstim
	bin/selestim -threads $(threads) -file $< -outputs $(OUTDIR)/SelEstim/Results -thin 20 -npilot 50 -burnin 10000 -length 1000000 -calibration
	
	# Modify the R script to print the outliers and the KLD plot
	sed 's|FILENAME_HERE|"$(OUTDIR)/SelEstim/$(RUN_NAME)_KLDs.svg"|g' skeletons/SelEstim.R > $(OUTDIR)/SelEstim/SelEstim.R
	
	# Run the R script provided by SelEstim with the output modifications
	Rscript -e 'source("$(OUTDIR)/SelEstim/SelEstim.R");plot.kld(file="$(OUTDIR)/SelEstim/Results/summary_delta.out", calibration_file="$(OUTDIR)/SelEstim/Results/calibration/summary_delta.out", limit=$(selestim_limit))' > $@
	

$(OUTDIR)/Baypass/$(RUN_NAME).baypass : $(OUTDIR)/Bayescan/$(RUN_NAME).geste
	# Convert geste to baypass format
	mkdir -p $(OUTDIR)/Baypass
	python3 bin/geste2baypass.py $< $@


$(OUTDIR)/Baypass/mcmc_aux/$(RUN_NAME)_mcmc_aux_summary_betai.out : $(OUTDIR)/Baypass/$(RUN_NAME).baypass $(OUTDIR)/ENVFILE $(OUTDIR)/$(RUN_NAME)_popnames_single.txt $(OUTDIR)/Bayescan/$(RUN_NAME).geste
	# Prepare the wrapper program by defining the required variables
	sed 's|source("")|source("$(wd)bin/baypass_utils.R")|' skeletons/Baypass_workflow.R > $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's|baypass_executable = ""|baypass_executable = "$(wd)bin/g_baypass"|' $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's|popname_file = ""|popname_file = "$(word 3,$^)"|' $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's|envfile = ""|envfile = "$(word 2,$^)"|' $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's|geno_file = ""|geno_file = "$(word 1,$^)"|' $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's|prefix = ""|prefix = "$(RUN_NAME)"|' $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's?num_pops =?num_pops = $(shell head -n 3 $(word 4,$^) | tail -n 1 |sed "s/.*=//")?' $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's?num_SNPs =?num_SNPs = $(shell head -n 1 $(word 4,$^) | sed "s/.*=//")?' $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's|num_threads =|num_threads = $(threads)|' $(OUTDIR)/Baypass/Baypass_workflow.R
	sed -i 's|scale_cov = TRUE|scale_cov = TRUE|' $(OUTDIR)/Baypass/Baypass_workflow.R
	
	# Run Baypass
	cd $(OUTDIR)/Baypass && Rscript Baypass_workflow.R

$(OUTDIR)/Baypass/$(RUN_NAME)_associations_summary.txt : $(OUTDIR)/Baypass/mcmc_aux/$(RUN_NAME)_mcmc_aux_summary_betai.out
	# Gather a summary of associations
	python3 bin/associations_gatherer.py $< $(ASSOC_BF) > $@


$(OUTDIR)/LFMM/$(RUN_NAME)_associations.csv : $(FINAL_VCF) $(OUTDIR)/LFMM_ENVFILE
	# Create a directory for LFMM
	mkdir -p $(OUTDIR)/LFMM
	# Prepare the wrapper script by defining the missing values
	sed 's|original_vcf = ""|original_vcf = "$<"|' skeletons/LFMM_workflow.R > $(OUTDIR)/LFMM/LFMM_workflow.R
	sed -i 's|target_lfmm = ""|target_lfmm = "$(subst .vcf,.lfmm,$(word 1,$^))"|' $(OUTDIR)/LFMM/LFMM_workflow.R
	sed -i 's|PCA_points = 5|PCA_points = "estimate"|' $(OUTDIR)/LFMM/LFMM_workflow.R
	sed -i 's|ENV_FILE = ""|ENV_FILE = "$(word 2,$^)"|' $(OUTDIR)/LFMM/LFMM_workflow.R
	sed -i 's|ASSOCIATION_TABLE = ""|ASSOCIATION_TABLE = "$@"|' $(OUTDIR)/LFMM/LFMM_workflow.R
	
	# Run LFMM
	cd $(OUTDIR)/LFMM && Rscript LFMM_workflow.R


$(OUTDIR)/Summary/LFMM_summary.txt : $(OUTDIR)/LFMM/$(RUN_NAME)_associations.csv
	# Create a summary directory
	mkdir -p $(OUTDIR)/Summary
	# Extract a summary of significant LFMM associations
	python3 bin/lfmm_table_to_summary.py $< $(LFMM_alpha) $@


$(OUTDIR)/LFMM2/$(RUN_NAME)_associations.csv : $(FINAL_VCF) $(OUTDIR)/LFMM_ENVFILE
	# Create a directory for LFMM2
	mkdir -p $(OUTDIR)/LFMM2
	# Prepare the wrapper script by defining the missing values
	sed 's|original_vcf = ""|original_vcf = "$<"|' skeletons/LFMM2_workflow.R > $(OUTDIR)/LFMM2/LFMM2_workflow.R
	sed -i 's|target_lfmm = ""|target_lfmm = "$(subst .vcf,.lfmm,$(word 1,$^))"|' $(OUTDIR)/LFMM2/LFMM2_workflow.R
	sed -i 's|PCA_points = 5|PCA_points = "estimate"|' $(OUTDIR)/LFMM2/LFMM2_workflow.R
	sed -i 's|ENV_FILE = ""|ENV_FILE = "$(word 2,$^)"|' $(OUTDIR)/LFMM2/LFMM2_workflow.R
	sed -i 's|ASSOCIATION_TABLE = ""|ASSOCIATION_TABLE = "$@"|' $(OUTDIR)/LFMM2/LFMM2_workflow.R
	
	# Run LFMM
	cd $(OUTDIR)/LFMM2 && Rscript LFMM2_workflow.R


$(OUTDIR)/Summary/LFMM2_summary.txt : $(OUTDIR)/LFMM2/$(RUN_NAME)_associations.csv
	# Create a summary directory
	mkdir -p $(OUTDIR)/Summary
	# Extract a summary of significant LFMM associations
	python3 bin/lfmm_table_to_summary.py $< $(LFMM_alpha) $@


$(OUTDIR)/Summary/Baypass_summary.txt : $(OUTDIR)/Baypass/$(RUN_NAME)_associations_summary.txt
	# Create a summary directory
	mkdir -p $(OUTDIR)/Summary
	# Extract a summary of significant Baypass associations
	cp $< $@


$(OUTDIR)/Summary/Bayescan_summary.txt : $(OUTDIR)/Bayescan/$(RUN_NAME)_outliers.txt
	# Create a summary directory
	mkdir -p $(OUTDIR)/Summary
	# Extract a summary of significant Bayescan outliers (turn an R print into a file with a single value per line)
	grep "\[" $< | head -n -1 | sed 's|\[.*\]||; s|\s|\n|g' |sed '/^\s*$$/d' > $@


$(OUTDIR)/Summary/SelEstim_summary.txt : $(OUTDIR)/SelEstim/$(RUN_NAME)_outliers.txt
	# Create a summary directory
	mkdir -p $(OUTDIR)/Summary
	# Extract a summary of significant SelEstim outliers (turn an R print into a file with a single value per line)
	sed 's|\[.*\]||; s|\s|\n|g' $< | sed '/^\s*$$/d' > $@


$(OUTDIR)/Summary/SNP_count.txt : $(FINAL_VCF)
	# Counts the number of SNPs in the used VCF file
	grep -c -v "^#" $< > $@


$(OUTDIR)/Summary/Sample_count.txt : $(FINAL_VCF)
	# Counts the number of samples in the used VCF vile
	grep -m 1 "^#CHROM" $< | cut -f 10- |wc -w > $@


# "Meta" rules
.PHONY : all clean CHELSA filtering outliers associations summary

clean :
	# Clear EVERYTHING!
	rm -rf $(OUTDIR)/*

filtering : $(FINAL_VCF) $(OUTDIR)/popinfo_names.txt

outliers : $(OUTDIR)/Bayescan/$(RUN_NAME)_outliers.txt $(OUTDIR)/SelEstim/$(RUN_NAME)_outliers.txt

associations : $(OUTDIR)/Baypass/$(RUN_NAME)_associations_summary.txt $(OUTDIR)/LFMM/$(RUN_NAME)_associations.csv $(OUTDIR)/LFMM2/$(RUN_NAME)_associations.csv

summary : $(OUTDIR)/Summary/LFMM_summary.txt $(OUTDIR)/Summary/LFMM2_summary.txt $(OUTDIR)/Summary/Baypass_summary.txt $(OUTDIR)/Summary/Bayescan_summary.txt $(OUTDIR)/Summary/SelEstim_summary.txt $(OUTDIR)/Summary/SNP_count.txt $(OUTDIR)/Summary/Sample_count.txt
