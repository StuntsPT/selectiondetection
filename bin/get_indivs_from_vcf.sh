#!/bin/bash

set -e

grep -m 1 "^#CHROM" $1 |cut -f 10- |tr "\t" "\n" > $2

