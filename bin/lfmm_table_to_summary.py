#!/usr/bin/env python3
"""
Script to read results from a LFMM table, look for significant associations and
report them in a fashion similar to that of Baypass
"""


from sys import argv


def main(csv_file, alpha):
    """
    Parses a csv table and returns a lit with coordinates of significant values
    """
    infile = open(csv_file, "r")
    line_num = 0
    match = []
    for lines in infile:
        line_num += 1
        lines = lines.replace("NA", "1")
        lines = lines.split(",")
        lines = list(map(float, lines))
        match += ["%s\t%s\t%s" % (lines.index(x) + 1, line_num, x)
                  for x in lines if x < alpha]

    infile.close()

    return match


def matches_writer(matches, results_file):
    """
    Writes a results file based on the found matches
    """
    outfile = open(results_file, "w")
    for i in sorted(matches):
        outfile.write(i + "\n")

    outfile.close()


if __name__ == "__main__":
    MATCHES = main(argv[1], float(argv[2]))
    matches_writer(MATCHES, argv[3])
