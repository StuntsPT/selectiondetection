#!/usr/bin/env python3

# Copyright 2018 Francisco Pina Martins <f.pinamartins@gmail.com>
# This file is part of Limonium_GBS_data_analyses.
# structure_threader is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# structure_threader is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Limonium_GBS_data_analyses. If not, see <http://www.gnu.org/licenses/>.

# Usage: python3 indiv_to_merged_pops.py pop_map.tsv env_vars_file.txt

import numpy as np
from collections import OrderedDict

def parse_pop_map(pop_map_filename):
    """
    Parses a pop_map file and returns a dict {indiv_name : pop, ...}
    """
    infile = open(pop_map_filename, "r")
    map_dict = {}
    for lines in infile:
        lines = lines.strip().split()
        map_dict[lines[0]] = lines[1]

    infile.close()
    return map_dict


def env_vars_parser(env_vars_filename, map_dict):
    """
    Parses the env vars file and returns a dict
    {pop_name : [[indiv_env_vars], [indiv_env_vars], ...]}
    """
    infile = open(env_vars_filename, "r")
    pop_dict = OrderedDict()
    header = infile.readline()  # Get header
    for lines in infile:
        lines = lines.strip().split()
        pop = map_dict[lines[0]]
        if pop in pop_dict:
            pop_dict[pop] += [lines[1:]]
        else:
            pop_dict[pop] = [lines[1:]]

    infile.close()
    return pop_dict


def make_averages(pop_dict):
    """
    Reads a pop_dict and returns the average values of each env var per population
    {pop_name : [avg_env_vars]}
    """
    averages = OrderedDict()
    for pops, env_vars in pop_dict.items():
        var_array = np.array(env_vars, dtype=float)
        averages[pops] = np.round(np.average(var_array, axis=0), 4)

    return(averages)


if __name__ == "__main__":
    from sys import argv
    map_dict = parse_pop_map(argv[1])
    pop_dict = env_vars_parser(argv[2], map_dict)
    for pop, env_vars in make_averages(pop_dict).items():
        print(pop + "\t" + "\t".join([str(x) for x in env_vars]))
