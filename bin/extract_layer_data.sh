#!/bin/bash

set -e

maps_dir=$3

for i in $(ls -v1 ${maps_dir}/*.tif)
do
	echo "Processing ${i}..."
	python3 bin/layer_data_extractor.py $1 ${i} |cut -f 2 |paste ${maps_dir}/temp_holder.tsv - > $2
	cp $2 ${maps_dir}/temp_holder.tsv
done
rm ${maps_dir}/temp_holder.tsv
