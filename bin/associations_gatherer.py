#!/usr/bin/env python3


def baypass_summary_betai_parser(summary_filename, bf_treshold):
    """
    Parses a baypass summary file to extract any significant associations
    between a marker and a covariate.
    Returns a set with associated SNP numbers
    """
    summary = open(summary_filename, 'r')
    header = summary.readline()  # Skip header and get BF column.
    for identifier in ("eBPmc", "BF(dB)"):
        try:
            bf_col = header.strip().split().index(identifier)
        except ValueError:
            pass
    for lines in summary:
        splitline = lines.strip().split()
        bf_value = float(splitline[bf_col])
        if bf_value >= float(bf_treshold):
            print("\t".join(splitline[:2]) + "\t" + splitline[bf_col])

    summary.close()


if __name__ == "__main__":
    from sys import argv
    baypass_summary_betai_parser(argv[1], argv[2])
