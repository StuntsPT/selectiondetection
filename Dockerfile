FROM ubuntu:18.04
# Use CRAN to get latest R version
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get -qq update && apt-get install -y --no-install-recommends \
    gnupg2 \
    ca-certificates
RUN echo "deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran40/" >> /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9

# Install repository tools - python3, pip3, curl, JRE, wget, R and VCFTools
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get -qq update && apt-get install -y --no-install-recommends \
    bc \
    curl \
    g++ \
    gcc \
    gfortran \
    libblas-dev \
    liblapack-dev \
    make \
    file \
    openjdk-8-jre-headless \
    python3-pip \
    python3-setuptools \
    python3-wheel \
    python3-gdal \
    r-base \
    vcftools \
    libcurl4-openssl-dev \
    libssl-dev \
    libxml2-dev \
    graphviz \
    wget && \
    ln -fs /usr/share/zoneinfo/Europe/Lisbon /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    apt-get clean

## Install software not available in Ubuntu repositories
WORKDIR /SD/software

# Allow R to use multi-core
RUN echo "options(Ncpus = 8)" >> ~/.Rprofile

# Install Bayescan
RUN curl http://cmpg.unibe.ch/software/BayeScan/files/BayeScan2.1.zip -L -o bayescan.zip;\
    unzip bayescan.zip;\
    mv BayeScan2.1/binaries/BayeScan2.1_linux64bits ./BayeScan;\
    chmod +x BayeScan;\
    rm -rf BayeScan2.1 bayescan.zip

# Install SelEstim
RUN curl http://www1.montpellier.inra.fr/CBGP/software/selestim/files/SelEstim_1.1.7.zip -L -o selestim.zip;\
    unzip /SD/software/selestim.zip;\
    cd SelEstim_1.1.7/src;\
    make all;\
    mv selestim ../../;\
    rm -rf SelEstim_1.1.7 selestim.zip

# Install SelEstim R-dependencies
RUN R -e "install.packages('IDPmisc', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('gplots', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('fields', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('maps', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('geigen', repos='https://cloud.r-project.org/')"

# Install Baypass
RUN curl http://www1.montpellier.inra.fr/CBGP/software/baypass/files/baypass_2.2.tar.gz -L -o baypass.tar.gz;\
    tar xvfz baypass.tar.gz;\
    cd baypass_2.2/sources;\
    make FC=gfortran;\
    mv ./g_baypass ../../;\
    rm -rf baypass_2.2 baypass.tar.gz

# Install Baypass R dependencies
RUN R -e "install.packages('corrplot', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('ape', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('mvtnorm', repos='https://cloud.r-project.org/')"

# Install PGDSpider
RUN curl http://www.cmpg.unibe.ch/software/PGDSpider/PGDSpider_2.1.1.5.zip -L -o pgdspider.zip;\
    unzip pgdspider.zip;\
    mv PGDSpider_2.1.1.5/PGDSpider2-cli.jar ./;\
    rm -rf PGDSpider_2.1.1.5 pgdspider.zip

# Install LFMM and dependencies
RUN R -e "install.packages('devtools', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('foreach', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('RSpectra', repos='https://cloud.r-project.org/')"\
    && R -e "install.packages('nFactors', repos='https://cloud.r-project.org/')"\
    && R -e "devtools::install_github('bcm-uga/lfmm')"\
    && R -e "install.packages('BiocManager', repos='https://cloud.r-project.org/')"\
    && R -e "BiocManager::install('LEA', update=F)"

## The good stuff!
# Get the analyses framework
WORKDIR /SD
COPY . ./

